

var canvas, c, w, h, a, mouse, audio, audioCtx, source,analyser, frequencyData, deformation1, deformation2;


//-------------------------------------------LETTERS AS SVG PATH

var rSVG = "m 4.3171835,4.5543847 q 0.3358968,0.1136881 0.651123,0.4857584 0.3203939,0.3720703 l 0.6407878,1.0231933 L 6.6684611,8.1717348 H 5.5470826 L 4.5600627,6.1925275 Q 4.1776571,5.4173811 3.8159221,5.1641665 3.4593548,4.910952 2.8392376,4.910952 H 1.7023561 V 8.1717348 H 0.65849221 V 0.4564437 H 3.0149374 q 1.3229167,0 1.9740397,0.5529378 0.651123,0.5529378 0.651123,1.6691487 0,0.7286377 -0.3410644,1.2092285 -0.3358968,0.4805908 -0.9818522,0.666626 z M 1.7023561,1.3142725 v 2.7388508 h 1.3125813 q 0.7544759,0 1.1368815,-0.3462321 0.3875733,-0.3513997 0.3875733,-1.028361 0,-0.6769612 -0.3875733,-1.0180257 -0.3824056,-0.346232 -1.1368815,-0.346232 z";

var aSVG = "M 6.9162706,2.1012456 L 4.1280711,9.6619473 h 5.586575 z M 5.7562168,0.07623938 H 8.0865004 L 13.876594,15.268874 H 11.739652 L 10.355728,11.3715 H 3.5073405 l -1.3839238,3.897374 h -2.16746903 z";



var zSVG = "M 15.102988,-0.09772921 V -1.0396459 l 4.60375,-5.7467498 h -4.169834 v -0.889 h 5.439834 v 0.889 l -4.720167,5.79966651 h 4.836583 v 0.88899998 z";

var letters = { "A" : svg2json(aSVG,10), "Z" : svg2json(zSVG,40), "R" : svg2json(rSVG,20) };


var colors = [
	{ r: 200, g: 100, b: 0},
	{ r: 100, g: 200, b: 0},
	{ r: 200, g: 0, b: 100},
	{ r: 100, g: 100, b: 100},
	{ r: 150, g: 0, b: 150},
];
var bg = colors[0];
var iColor = 0;



//----------------------------------------------------INIT
document.querySelector("body").onload = function() {
	canvas = document.querySelector("canvas");
	c = canvas.getContext("2d");

	mouse = { x: 0, y: 0 };
	canvas.onmousemove = function(event) {
		mouse = { x: event.clientX, y: event.clientY };
	}

	window.onresize = resize;
	resize();



	audio = document.querySelector("audio");
	audioCtx = new AudioContext();
	analyser = audioCtx.createAnalyser();
	source = audioCtx.createMediaElementSource(audio);
	source.connect(analyser);
	analyser.connect(audioCtx.destination);
	analyser.fftSize = 256;
	audio.play();
	frequencyData = new Uint8Array(analyser.frequencyBinCount);



	window.requestAnimationFrame(draw);
	// setInterval(draw,10);
}

//-----------------------------------------CANVAS SIZE
function resize() {
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	w = canvas.width;
	h = canvas.height;
}

//----------------------------------------------SVG -> JSON
function svg2json(svg, factor) {
	var actionsNames = {
		"m" : "move",
		"q" : "quadratic",
		"c" : "bezier",
		"l" : "line",
		"h" : "horizontal",
		"v" : "vertical",
		"z" : "stop",
		"M" : "move",
		"Q" : "quadratic",
		"C" : "bezier",
		"L" : "line",
		"H" : "horizontal",
		"V" : "vertical",
		"Z" : "stop",
	};
	var action = null;
	var json = { width: 0, height: 0, actions: [] };
	var prevPoint = { x: 0, y: 0 };
	var firstPoint = null;  
	for(let value of svg.split(" ")) {
		if (actionsNames[value]) {
			if (action)
				json.actions.push(action);
			if (actionsNames[value] == "stop") {
				json.actions.push({ type: "line", points: [firstPoint] });				 
				prevPoint = firstPoint;
				firstPoint = null;
			}
			action = { type: actionsNames[value], points: [], relative: value.toLowerCase()==value };
		} else {
			var point = value.split(",");
			for(i in point)
				point[i] = eval(point[i])*factor;
			if (action.type=="horizontal")
				point = { x: point[0]+ (action.relative?prevPoint.x:0), y: prevPoint.y };
			else if (action.type=="vertical")
				point = { x: prevPoint.x, y: point[0]+(action.relative?prevPoint.y:0) }
			else
				point = { x: point[0]+(action.relative?prevPoint.x:0), 
					      y: point[1]+(action.relative?prevPoint.y:0)};
			if (point.x > json.width) json.width = point.x;
			if (point.y > json.heigth) json.height = point.y;

			action.points.push(point);
			if (!firstPoint)
				firstPoint = point;
			prevPoint = point;
		}
	}
	json.actions.push(action);
	return json;
}


//----------------------------------------------------MAIN LOOP (DRAW)
function draw() {
	analyser.getByteFrequencyData(frequencyData);
	c.clearRect(0,0,w,h);
	c.beginPath();
	c.fillStyle = 'rgb('+bg.r+', '+bg.g+', '+bg.b+')';
	next = (iColor+1)%colors.length;
	if (colors[next].r != bg.r)
		bg.r = bg.r + (colors[next].r-bg.r) / Math.abs(colors[next].r-bg.r);
	if (colors[next].g != bg.g)
		bg.g = bg.g + (colors[next].g-bg.g) / Math.abs(colors[next].g-bg.g);
	if (colors[next].b != bg.b)
		bg.b = bg.b + (colors[(next)%colors.length].b-bg.b) / Math.abs(colors[next].b-bg.b);
	if (colors[next].r == bg.r && 
		colors[next].g == bg.g  && 
		colors[next].b == bg.b)
		iColor = next;
	
	c.fillRect(0,0,w,h);

	//------------------------------ 4 MAIN FREQUENCIES
	var sums = [0,0,0,0];
	for (i in frequencyData) {
		var freq = frequencyData[i];
		sums[Math.floor(4*i/frequencyData.length)] += freq;
	}

	c.beginPath();
	c.strokeStyle = "white";
	c.fillStyle = "rgba(0,0,0,.5)";
	c.strokeStyle = 'black';
	var wBar = w/frequencyData.length;
	for (i in frequencyData) {
		var freq = frequencyData[i] ;
		c.rect(i*wBar,h/5-freq/2,wBar-2,freq);
		i++;
	}
	c.fill();
	// c.stroke();



	c.lineWidth = 2;
	deformation1 = sums[0];
	deformation2 = sums[1]+sums[2];

	var letter = letters["A"];
	drawLetter(letter,w/2-letter.width/2,h/2-letter.height, 1);


	window.requestAnimationFrame(draw);

}


//-------------------------------------------------DRAW LETTER
function drawLetter(letter,x,y, vectorWay) {
	c.save();
	c.translate(x,y);
	c.beginPath();
	c.strokeStyle =  "white";
	var last = {x: 0, y: 0 };
	var signe = -1;
	for(let action of letter.actions) {
		var current;
		if (action.points.length > 0)
			current = { x: action.points[0].x, y: action.points[0].y };

		var vector = { x: current.x-last.x, y: current.y-last.y };
		var vectorLength = Math.sqrt(Math.pow(vector.x,2)+Math.pow(vector.y,2));
		var normalVector = { x: vector.y, y: -vector.x };

		var localDeformation1 = vectorWay * signe * deformation1 * vectorLength / 3000000;
		var localDeformation2 = vectorWay * signe * deformation2 * vectorLength / 500000;

		if (action.type == "stop") {
			c.stroke();  
			c.beginPath();  
			signe = -signe;
		} else if (action.type == "move") {
			c.moveTo(current.x, current.y); 
		} else if (action.type == "line" || action.type == "horizontal" || action.type == "vertical") {
			c.bezierCurveTo(
				last.x + localDeformation2*normalVector.x + localDeformation1*vector.y, 
				last.y + localDeformation2*normalVector.y + localDeformation1*vector.x, 
				current.x + localDeformation1*normalVector.x,
				current.y + localDeformation1*normalVector.y, 
				current.x, current.y); 
		}
		last = current;
	}
	c.restore();
}





//-------------------------------------------------DRAW LETTER
// function drawLetter(letter,x,y) {
// 	c.save();
// 	c.translate(x,y);
// 	c.beginPath();
// 	c.strokeStyle =  "white";
// 	for(let action of letters[letter]) {
// 		switch (action.type) {
// 			case "stop"		  : c.stroke();  c.beginPath();   break;
// 			case "move"       : c.moveTo(action.points[0].x,action.points[0].y); break;
// 			case "line"       : c.lineTo(action.points[0].x,action.points[0].y); break;
// 			case "vertical"   : c.lineTo(action.points[0].x,action.points[0].y);  break;
// 			case "horizontal" : c.lineTo(action.points[0].x,action.points[0].y);  break;
// 			case "quadratic"  : 
// 								c.bezierCurveTo(
// 									action.points[0].x,action.points[0].y,
// 									action.points[1].x,action.points[1].y,
// 									action.points[2].x,action.points[2].y,
// 									// action.points[3].x,action.points[3].y,
// 								);
// 								break;
// 		}
// 	}
// 	c.restore();
// }




